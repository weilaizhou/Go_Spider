package main

import (
	"xiaoshuo/Parser"
	"xiaoshuo/engine"
	"xiaoshuo/fetcher"
	"xiaoshuo/persist"
	"xiaoshuo/scheduler"
)

func main(){
	body, _ := fetcher.Fetch("https://www.17k.com/all/book/2_29_0_0_0_0_0_1_1.html")
	itemChan, err := persist.ItemSaver("xiaoshuo_profile")
	if err != nil {
		panic(err)
	}
	// 解析出小说类别作为入口进行整站爬取
	request := Parser.ParserKind(body)
	e := engine.ConcurrentEngine{
		Scheduler: &scheduler.QueueScheduler{},
		WorkerCount: 100,
		ItemChan: itemChan,
	}
	e.Run(request...)
}

