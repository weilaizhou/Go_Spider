package Parser

import (
	"github.com/PuerkitoBio/goquery"
	"log"
	"regexp"
	"strconv"
	"xiaoshuo/engine"
)

// 正则匹配出页数
var numRe = regexp.MustCompile(`共 ([0-9]+) 页`)
// 解析每种类型的小说数量
func NumberParser(document *goquery.Document, baseUrl string) engine.ParserResult {
	result := engine.ParserResult{}
	baseUrl = baseUrl[0 : len(baseUrl) - 6]
	text := document.Find(".page").Text()
	match := numRe.FindSubmatch([]byte(text))
	if len(match) >= 2 {
		count, err := strconv.Atoi(string(match[1]))
		if err != nil {
			log.Printf("String to Number failed!")
			return engine.ParserResult{}
		}
		for i := 1; i <= count; i++{
			url := baseUrl + strconv.Itoa(i) + ".html"
			result.Request = append(result.Request, engine.Request{
				Url: url,
				ParserFunc: ContentParse,
			})
		}
	}
	return result
}
