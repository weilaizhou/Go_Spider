package Parser

import (
	"github.com/PuerkitoBio/goquery"
	"xiaoshuo/engine"
	"xiaoshuo/utils"
)

const baseUrl = "https://www.17k.com"

func ParserKind(document *goquery.Document) []engine.Request {
	urls := []string{}
	result := []engine.Request{}
	document.Find(".allzplb a").Each(func(i int, s *goquery.Selection){
		url, _ := s.Attr("href")
		urls = append(urls, baseUrl + url)
	})
	urls = utils.RemoveRepByMap(urls)
	for _, m := range urls {
		result = append(result, engine.Request{
			Url: m,
			ParserFunc: func(doc *goquery.Document) engine.ParserResult{
				return NumberParser(doc, m)
			},
		})
	}
	return result
}