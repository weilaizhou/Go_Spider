package engine

import "github.com/PuerkitoBio/goquery"

type Request struct {
	Url string      // 当前需要执行爬虫任务的请求地址
	ParserFunc func(document *goquery.Document) ParserResult    //该地址请求回来的网页内容对应的函数解析器
}

type ParserResult struct {
	Request []Request
	Item Profile
}


type Profile struct {
	Id string
	Type string
	Cover string    // 小说封面
	Title string    // 小说标题
	Author string   // 小说作者
	Kind   string   // 小说类型
	WordNum  int    // 小说字数
	Desc     string // 小说简介
	Updated  string // 最近更新时间
	Url      string // 小说观看地址
}

func NilParser(*goquery.Document) ParserResult {
	return ParserResult{}
}