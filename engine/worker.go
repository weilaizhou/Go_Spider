package engine

import (
	"log"
	"xiaoshuo/fetcher"
)

func worker(r Request) (ParserResult, error) {
	//log.Printf("Fetching %s", r.Url)
	// 调用fetch发起请求
	body, err := fetcher.Fetch(r.Url)
	if err != nil {
		log.Printf("Fetch: Error Fetching url %s: %v", r.Url, err)
		return ParserResult{}, err
	}

	return r.ParserFunc(body), err
}
