package persist

import (
	"context"
	"encoding/json"
	"github.com/olivere/elastic"
	"testing"
	"xiaoshuo/engine"
)

func TestSave(t *testing.T){
	profile := engine.Profile{
		Id: "3136300",
		Cover: "http://cdn.static.17k.com/book/189x272/00/63/3136300.jpg-189x272?v=0",
		Title: "记忆青涩的美好时光",
		Author: "夜色明",
		Kind: "纯爱青春",
		WordNum: 1658,
		Desc: "记忆里的青春，是甜的、苦的、酸的，又或是红的、黄的、蓝的。每一个人，几乎都将自己最热血的年纪、最唯美的感情放在了学校中，也在成长的岁月里，任它们消逝在蹉跎的岁月。任时光流淌，记忆中模糊的青春岁月，会随着校园时代的过去，而愈发深刻，终于化为内心永恒的浪漫月光林星：记忆我青涩的美好时光。",
		Updated: "2020-04-10 13:20",
		Url: "https://www.17k.com/book/3136300.html",
	}
	client, err := elastic.NewClient()
	if err != nil {
		panic("Open elastic fiald.")
	}
	id, err := save(client, profile,"xiaoshuo_profile")
	if err != nil {
		panic(err)
	}
	// TODO: try to save profile
	client, err = elastic.NewClient()
	if err != nil {
		panic(err)
	}
	resp, err := client.Get().Index("xiaoshuo_profile").Type("xiaoshuo").Id(id).Do(context.Background())
	if err != nil{
		panic(err)
	}

	var actual engine.Profile
	err = json.Unmarshal([]byte(resp.Source), &actual)
	if err != nil {
		panic(err)
	}
	if actual != profile {
		t.Errorf("Got %v; expected %v", actual, profile)
	}
}